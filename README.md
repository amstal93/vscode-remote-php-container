# vscode remote php container

A dev container for PHP development with
[vscode](https://aka.ms/vscode-remote/containers/getting-started)

## Composer

Composer is installed and can be used inside the container.
The user's local composer dir is mounted to enable a persistent cache and share global configuration.

## SSH

The user's local ssh settings are mounted into the container.
This should avoid issues with host key verification.

## phpcs

The PHP_CodeSniffer is installed and can be used directly.

> phpcs .

> phpcbf .
